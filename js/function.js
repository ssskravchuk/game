/*=================================
Функции для создания элементов игры
=================================*/
/*
<div id="start-block">
	<button id="start-khopka">Начать</button>			
</div>	
*/
// Создание блока старта игры
function sozdanieStartBlock() {
    // создаем блок div - <div id="start-block">
	startBlock = document.createElement("div");
	// присваиваем ему id имя - id="start-block"
	startBlock.id = "start-block";
	// создаем кнопку - <button id="startBtn">Начать</button>
	startKnopka = document.createElement("button");
	// присваиваем ей id имя - id="startBtn"
	startKnopka.id = "star-knopka";
	// прописываем текст для кнопки - "Начать"
	startKnopka.innerText = "Начать";
	// добавляем кнопку в стартовый блок
	startBlock.appendChild(startKnopka);
	// добавляем стартовый блок на игровое поле
	igraPole.appendChild(startBlock);
}

/*
<div id="stars">0<div>
*/
// Создание блока очков
function sozdanieOchkiBlock() {
	// создаем блок div - <div id="stars">0<div>
	stars = document.createElement("div");
	stars.id = "stars";
	stars.innerText = ochki;
    // добавляем блок очков на игровое поле
	igraPole.appendChild(stars);
	// создаем функцию обнуления счетчика при клике по блоку очков
	stars.onclick = function() {
		ochki = 0;
		stars.innerText = "0";
	}
}

/*
<div id="lifes"><span></span></div>
*/
// Создание блока жизней
function sozdanieLifesBlock() {
	// создаем блок div - <div id="lifes"><span></span></div>
	lifes = document.createElement("div");  
	lifes.id = "lifes";		
	// создаем переменную с текущим колличеством отобоаженных жизней
    var tekusheecolichestvoLifes = 0; 
	// создаем цикл while - пока выполняется условие работает цикл   
    while (tekusheecolichestvoLifes < colichestvoLifes) {  // условие для работы цикла
    	span = document.createElement("span"); // создаем элемент span
        lifes.appendChild(span); // добавляем элемент span в блок жизней		     	 		
    	tekusheecolichestvoLifes = tekusheecolichestvoLifes + 1; // останавливаем цикл		   	
    }	
    // добавляем блок жизней на игровое поле	  
    igraPole.appendChild(lifes);  
}

/*
<div id="blockInfo">
    <div id="infoBlock">
        <h2>Время игры: <span id="timer">100</span></h2>
    </div>
</div>
*/
// Создания блока таймер 
function sozdanieTimerBlock() {
	// создаем блок div - <div id="infoBlock">
	// <h2>Время игры: <span id="timer">100</span></h2></div>
	infoBlock = document.createElement("div");
	infoBlock.id = "infoBlock";
	// создаем заголовок h2 с текстом "Время:"
	var h2 = document.createElement("h2");  
	    h2.innerText = "Время: ";  
	// объявляем переменную timerBlock - добавляем тег span
	timerBlock = document.createElement("span"); 
	timerBlock.id = "timer"; // присватваем id имя	
	timerBlock.innerText = "100"; // добавляем в блок span текст "100"
	h2.appendChild(timerBlock); // добавляем тег span в заголовок h2
	// добавляем в информационный блок заголовок с таймером	        
	infoBlock.appendChild(h2);
	// добавляем блок infoBlock на страницу
	blockInfo.appendChild(infoBlock); 		
}

/*
<div class="ball ltft"><div>
<div class="ball right"><div>
<div class="ball bomb"></div>
*/
// Создания шарика 
function sozdanieBall() {
	// создаем блок div
	var ball = document.createElement("div");  
	    // присваиваем шарику имя класса - className
	    ball.className = "ball";
	// создаем переменную и ложим в нее случайное число от 1 до 5    
	var kakoyBall = random(5);
	// создаем перменную для выбора кокой шарик помещать на поле, если 1:
	if (kakoyBall == 1) {
		// помещаем на поле бомбочку
		ball.className = "bomb";		
		// выбираем случайное еаправление для вылета бомбочки
	    var napravlenie = random(2); // 1 - left, 2 - right
        // проверяем какое значение получили при случайном выборе
	    if (napravlenie == 1) {
		    // если 1 - left, присваиваем имя класса - ball bomb left
		    ball.className = "ball bomb left";		
	    } else {
		    // если 2 - right, присваиваем имя класса - ball bomb right
		    ball.className = "ball bomb right";		
	    }
	// иначе, если число не 1:     		
	} else {
		// помещаем на поле шарик
		ball.className = "ball";
		// выбираем случайное еаправление для вылета шарика
	    var napravlenie = random(2); // 1 - left, 2 - right
        // проверяем какое значение получили при случайном выборе
	    if (napravlenie == 1) {
		    // если 1 - left, присваиваем имя класса - ball left
		    ball.className = "ball left";		
	    } else {
		    // если 2 - right, присваиваем имя класса - ball right
		    ball.className = "ball right";		
	    } 		
	} 

    // создаем функцию при условии наведения на шарик (клик заменяем на наведение)
	ball.onmousemove = function() {		
		// что-бы избежать многократного выполнения функции при наведении на шарик,
		// создаем условие - если при наведении на шарик его className не равен значению 
		// "ball ozidaet-udalenie", запускаем цикл создания шариков, 
		// иначе присваиваем шарику класс "ball ozidaet-udalenie" (строка 184)
		if (ball.className != "ball ozidaet-udalenie") {
			// создаем условие, если шарик бомбочка - не меняем колличество очков
			if ((ball.className == "ball bomb right") || (ball.className == "ball bomb left")) {
				ochki = ochki;
    		} else { 
    			// иначе, если шарик обычный - увеличиваем очки на случайное значение от 1 до 5
				ochki = ochki + random(5);
    		}			
	        // меняем текст счета на значение ochki
	        stars.innerText = ochki;  
            // делаем удаление плавным
	        ball.style.opacity = "0";     
			// запускаем таймер
			setTimeout(function() {
        	    ball.remove();  // удаляем мячик
                // создаем коробочку и помещаем в нее шарики, которые есть на игровом поле 
        	    var sushestvuetBall = document.querySelector(".ball"); // element | null
                // проверяем условие - ести ли шарики на игровом поле
                // если шариков нет (равно null), выполняем условие
        	    if (sushestvuetBall == null) {
        	    	// сколько мячей хотим создать
    	            var colichectvoBall = random(5);
                    // текущее колличество мячей
                    var tekucheeColichectvoBall  = 0;
                    // создаем цикл while - пока выполняется условие работает цикл
                    while (tekucheeColichectvoBall < colichectvoBall) {
    	                sozdanieBall();  // вызываем функцию создания мяча
    	                // меняем текущее к-во очков, +1, иначе цикл будет продолжаться вечно
    	                tekucheeColichectvoBall = tekucheeColichectvoBall + 1;
                    } // конец цикла while                    
        	    } // конец условия  if (sushestvuetBall == null)              
            }, 200);  // конец таймера
            // cоздаем условие - если вылетевший шарик бомбочка (прописываем для left и right)            
            if ((ball.className == "ball bomb right") || (ball.className == "ball bomb left")) {
        	    // удаляем шарик
    			ball.remove();
    			// создаем шарик 
    			sozdanieBall();
    			// уменьшаем колличество жизней на 1 жизнь, при наведении курсора мыши
    			colichestvoLifes = colichestvoLifes - 1;
    			// создаем условие - если колличество жизней равно 0:
    			if (colichestvoLifes == 0) {
    				// заканчиваем игру
    				konecIgra();
    			}
    			// удаляем блок жизней, иначе будет появлятся несколько блоков жизней
    			udalenieLifesBlock();
    			// создаем блок жизней
    			sozdanieLifesBlock();
            }            
		} // конец условия if (ball.className != "ball ozidaet-udalenie")        
        // меняем класс шарика при наведении мыши
        ball.className = "ball ozidaet-udalenie";				
	} // конец события ball.onmousemove

	// запускаем таймер вызова функции расположения шарика на игровом поле
	setTimeout (function() {
		// случайное позиционирование по top
		ball.style.top = random(350) + "px";
		// случайное позиционирование по left  
		ball.style.left = random(550) + "px";  
	}, 200);  // таймер на 200 ms
    // запускаем таймер вызова функции падения шарика и его удаления, таймер на 1 сек.
    setTimeout(function() {
    	// меняем свойство изменения стилей - делаем задержку 0 сек. 
    	ball.style.transition = "all 0s";
    	// создаем переменную падения шарика с интервалом в 0,01 сек. (10 мс)	
    	var timerBall = setInterval(function() {
    		// меняем положение шарика по оси top на 1 px
    		ball.style.top = ball.offsetTop + 1 + "px";
    		// создаем условие - если top шарика больше 400, запускаем действия:
    		if (ball.offsetTop > 400) {    			
    			// удаляем шарик
    			ball.remove();
    			// создаем шарик 
    			sozdanieBall();    			
    			// создаем условие - если падающий шарик бомбочка не меняем колличество жизней 
    			if ((ball.className == "ball bomb right") || 
    				(ball.className == "ball bomb left")) {
    				colichestvoLifes = colichestvoLifes;
    			} else {
    				// иначе, уменьшаем колличество жизней на 1 жизнь, при падении каждого шарика
    				colichestvoLifes = colichestvoLifes - 1;
    			}   			
    			// создаем условие - если колличество жизней равно 0:
    			if (colichestvoLifes == 0) {
    				// заканчиваем игру
    				konecIgra();
    			}
    			// удаляем блок жизней, иначе будет появлятся несколько блоков жизней
    			udalenieLifesBlock();
    			// создаем блок жизней
    			sozdanieLifesBlock();
    			// по завершению цикла удаляем таймер
    			clearInterval(timerBall);
    		}
    	}, 10); // 10 миллисекунд (0,01 сек.)
    }, 1000); // 1000 мс (1 сек.)    
    // запускаем условие - добавляем шарик на игровое поле только если игра еще не закончена
	if (statusIgra != "koniec") {
		// помещаем шарик на игровое поле
	    igraPole.appendChild(ball);
	}
} // конец функции создания шариков

/*<div id="conec-igra">			
	<h2>Игра окончена!</h2>
	<h3>Вы набрали: 100 очков</h3>
	<button id="novai-igra">Начать новую игру</button>			
</div>*/
// Функция окончания игры
function sozdanieKonecIgraBlock() {
	// создаем блок div - <div id="conec-igra"></div>
    konecIgraBlock = document.createElement("div");    
    konecIgraBlock.id = "conec-igra";
    // создаем заголовок h2 - <h2>Игра окончена!</h2>
    var h2 = document.createElement("h2");    
        h2.innerText = "Игра окончена!";
    // создаем заголовок h3 - <h3>Вы набрали: 100 очков</h3>
    var h3 = document.createElement("h3");    
        h3.innerText = "Вы набрали: " + ochki + " очков";
    // создаем кнопку - <button id="novai-igra">Начать новую игру</button>
	btnNovaiIgra = document.createElement("button");
    btnNovaiIgra.id = "novai-igra";
    btnNovaiIgra.innerText = "Начать новую игру";	
    // помещаем в блок konecIgraBlock заголовок h2  
    konecIgraBlock.appendChild(h2);
    // помещаем в блок konecIgraBlock заголовок h3
    konecIgraBlock.appendChild(h3);
	// помещаем в блок konecIgraBlock кнопку "novai-igra"
	konecIgraBlock.appendChild(btnNovaiIgra);
    // помещаем в игровое поле блок konecIgraBlock
    igraPole.appendChild(konecIgraBlock);	
}

/*================
Удаление элементов
================*/
// Функция удаления старотового блока
function udalenieStartBlock() {		
	startBlock.remove();
}
// Функция удаления блока жизней
function udalenieLifesBlock() {   		
	lifes.remove();
}
// Функция удаления блока очков
function udalenieOchkiBlock() {		
	stars.remove();
}
// Функция удаления блока таймера
function udalenieTimerBlock() {		
	infoBlock.remove();
}
// Функция удаления блока окончания игры
function udalenieKonecIgraBlock() {
	konecIgraBlock.remove();
}
// Функция очистки игрового поля (если что-то забыли удалить)
function ochistitIgraPole() {
	igraPole.innerText = "";
}

/*=====================
Вспомогательніе функции
=====================*/
// Функция для получения случайного числа
function random(max) {
	// получаем случайное число от 1 до max (Max.random() - случайное число от 0 до 1)
	// +1 указывают для захвата последнего числа
	var sluchaynoeChislo = 1 + Math.random() * (max + 1); 
	// округляем до целого числа
	sluchaynoeChislo = Math.floor(sluchaynoeChislo);  
	// возвращаем случайное число (return - вернуть результат)
	return sluchaynoeChislo;
}



	
