// Главный файл, в котором вызываются все функции и действия игры
/*=================================
Функции для запуска действий
=================================*/

// запускаем функцию запуска игры
start();

// функция, которая выполняется при при загрузке страницы
function start() {	
    // запускаем функцию создания  блока старта игры    
	sozdanieStartBlock();
	// запускаем функцию создания  блока таймера 
	sozdanieTimerBlock(); 	
    // при клике на кнопку "Начать" запускаем игру
	startKnopka.onclick = nachat;
}

// функция, которая выполняется при запуске игры
function nachat() {	
	// задаем статус игры при запуске
	statusIgra = "nachat";		
	// запускаем функцию удаления стартового блока
	udalenieStartBlock();
	// запускаем функцию создания  блока очков 
	sozdanieOchkiBlock();
	// запускаем функцию создания  блока жизней 
	sozdanieLifesBlock();
	// запускаем функцию создания  шарика
	sozdanieBall();
	// запускаем функцию для обратного отсчета времени игры
    timerIgra();
}

// запускаем функцию окончания игры
function konecIgra() {
	// задаем статус игры при окончании
	statusIgra = "koniec";	
	// удаляем блок жизней
	udalenieLifesBlock();
	// удаляем блок очков
	udalenieOchkiBlock();
	// удаляем блок таймера
	udalenieTimerBlock();
	// удаляем все лишнее с игрового поля
	ochistitIgraPole();
	// завершаем игру
	sozdanieKonecIgraBlock();	
	// создаем функцию при клике на кнопку "Начать новую игру"
	btnNovaiIgra.onclick = function() {
		// возвоащаем начальное колличество жизней, для коректной работы цикла
	    colichestvoLifes = 5;
	    // возвоащаем начальное колличество очков, для коректной работы функции
	    ochki = 0;		
		// удаляем блок окончания игры		
		udalenieKonecIgraBlock();
		// создаем  блок таймера
		sozdanieTimerBlock();
		// задаем статус игры 
	    statusIgra = "nachat";	
	    // создаем  блок очков 
	    sozdanieOchkiBlock();
	    // создаем блок жизней 
	    sozdanieLifesBlock();
	    // создаем шарики
	    sozdanieBall();
	    // запускаем отсчет времени игры
        timerIgra();				
	}   	
}

// запускаем функция для обратного отсчета времени игры
function timerIgra() {
	// объявляем переменную chasy ложим в нее функцию setInterval
    var chasy = setInterval (function() {
        // значение блока #timer уменьшаем на 1 
    	timerBlock.innerText = timerBlock.innerText - 1;
    	// создаем условие, когда счетчик равен 0 
    	if (timerBlock.innerText == 0) {
    	    // останавливаем счетчик setInterval  
    		clearInterval(chasy);
    		// заканчиваем игру
    		konecIgra();						    		    		  
    	}
		// создаем условие, когда игра окончена
		if (statusIgra == "koniec") {
			// останавливаем счетчик setInterval
			clearInterval(chasy);
		}
    }, 1000);  // каждую секунду меняем текст блока #timer        
}



	




 