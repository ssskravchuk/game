// создаем переменную lifes - жизни на игровом поле
var lifes = null;

// создаем переменную colichestvoLifes - число жизней на игровом поле
var colichestvoLifes = 5;

// создаем переменную stars - очки на игровом поле
var starsIgra = null;

// создаем переменную счета игры ochki - присваиваем значение 0 
var ochki = 0;

// создаем переменную статуса игры
var statusIgra = "open";

// объявляем переменную startBlock - стартовый блок
var startBlock = null;

// объявляем переменную startBtn - кнопка нв стортовом поле - запуск игры
var startKnopka = null;

// объявляем переменную konecIgraBlock - блок конца игры
var konecIgraBlock = null;

// объявляем переменную btnNovaiIgra - кнопка на блоке конца игры - запуск новой игры 
var btnNovaiIgra = null;

// объявляем переменную timerBlock - блок таймера
var timerBlock = null;

// создаем переменную infoBlock - информационный блок
var infoBlock = null;

// создаем переменную blockInfo и ложим в нее блок #blockInfo
var blockInfo = document.querySelector("#blockInfo");

// создаем переменную igraPole - игровое поле и ложим в нее блок #igra
var igraPole = document.querySelector("#igra");